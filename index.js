var isLogin = true;
console.log("isLogin: ", isLogin);

var ss1 = 2 < 1;
console.log("ss1: ", ss1);
var ss2 = 2 >= 2;
console.log("ss2: ", ss2);
var ss3 = 2 <= 10;
console.log("ss3: ", ss3);
var ss4 = 2 == 10; //so sánh giá trị
console.log("ss4: ", ss4);
var ss5 = 2 === 10; //so sánh cả về kiểu dữ liệu và giá trị
console.log("ss5: ", ss5);

var ss6 = 2 > 1 && 3 > 5;
console.log("ss6: ", ss6);

// &&: chỉ đúng khi tất cả điều kiện đều đúng
// || :chỉ sai khi tất cả điều kiện đều sai